package errori;

public class FileSorgente extends File{
    private String contenuto;

    public String getContenuto() {
        return contenuto;
    }

    public void setContenuto(String contenuto) {
        this.contenuto = contenuto;
    }

    public FileSorgente(String nome, TipoFile tipoFile, String contenuto){
        super(nome, tipoFile);
        this.contenuto = contenuto;
    }

    public void aggiungiTesto(FileSorgente file1, String aggiunta){
        file1.setContenuto( file1.getContenuto()+"\n"+aggiunta );
    }

    public void stampaFileSorgente(FileSorgente file1){
        System.out.println(file1.getContenuto());
    }

    public void aggiungiTesto(FileSorgente file1, String aggiunta, int posizione) throws StringheVuote, PosizioneIrragiungibile{
        if ( posizione >= file1.getContenuto().length() || posizione < 0 )
            throw new PosizioneIrragiungibile("La stringa "+aggiunta+" non puo' essere aggiunta in posizione "+posizione);

        if ( aggiunta.length() == 0 )
            throw new StringheVuote("Non e' possibile aggiungere una stringa vuota");

        file1.setContenuto( file1.getContenuto().substring(0,posizione)+"\n"+
                aggiunta+"\n"+
                file1.getContenuto().substring(posizione,file1.getContenuto().length()) );

    }
}