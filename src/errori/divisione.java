package errori;
import java.util.Scanner;

public class divisione {
    public int getInteger(){
        System.out.println("Digitare un numero intero: ");
        Scanner in = new Scanner(System.in);
        return in.nextInt();
    }
    public void divisioneCasuale(int n){
        double d = Math.random();
        try {
            System.out.println(n + "/" + d + "=" + n/d);
        } catch (Exception e){
            System.out.println("Divisione per 0");
        }
    }
    public void divisione2(int n){
        double d = Math.random();
        if ( d == 0 ){
            throw new ArithmeticException("Il divisore non puo' essere 0");
        }
        else {
            System.out.println(n + "/" + d + "="+n / d);
        }
    }
    public void inserimentoOltreLimite(int[] array, int valore, int posizione){
        try{
            array[posizione] = valore;
        }catch (Exception e){
            System.out.println("Inserimento in una posizione inaccessibile");
        }
    }


    public void checkFormatOfImaginaryNumber(String number){

        try{
            double num = Double.parseDouble(number);
            System.out.println("E' un numero reale");
        }catch (Exception e){
            if ( number.charAt(0) == 'i'  )
                System.out.println("E' un numero immaginario");
            else
                System.out.println("Non e' un numero");
        }

    }

    public void stringheSimili(String a, String b, int n) throws StringheVuote, StringheDiverse{

        if ( n < 0 )
            throw new IllegalArgumentException("Numero errato (n deve essere >= 0)");

        if ( a.length() == 0 || b.length() == 0 )
            throw new StringheVuote("Stringhe vuote non ammesse");

        for ( int i=0; i<n; i++){
            if ( a.charAt(i) != b.charAt(i) )
                throw new StringheDiverse("Stringhe diverse");
        }

        System.out.println("Le stringhe sono uguali nei primi"+n+"caratteri");
    }

}