package errori;

public class PosizioneIrragiungibile extends Exception{
    public PosizioneIrragiungibile(){}

    public PosizioneIrragiungibile(String message){
        super(message);
    }
}
