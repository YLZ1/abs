package errori;

public class File {
    private String nome;
    private TipoFile tipoFile;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public TipoFile getTipoFile() {
        return tipoFile;
    }

    public void setTipoFile(TipoFile tipoFile) {
        this.tipoFile = tipoFile;
    }

    public File(String nome, TipoFile tipoFile){
        this.nome = nome;
        this.tipoFile = tipoFile;
    }
}
