package Negozio;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class NegozioAbbigliamento {
    private Map<Integer, Prodotto> prodottiDisponibili;     //integer = codice
    private Map<Integer, Cliente> clientiInNegozio;     //integer = codice cliente
    private ArrayList<Ordine> elencoOrdini;

    public List<Ordine> getElencoOrdini() {
        return elencoOrdini;
    }

    public void setElencoOrdini(ArrayList<Ordine> elencoOrdini) {
        this.elencoOrdini = elencoOrdini;
    }

    public Map<Integer, Prodotto> getProdottiDisponibili() {
        return prodottiDisponibili;
    }

    public Map<Integer, Cliente> getClientiInNegozio() {
        return clientiInNegozio;
    }

    public void setProdottiDisponibili(Map<Integer,
                                Prodotto> prodottiDisponibili) {
        this.prodottiDisponibili = prodottiDisponibili;
    }

    public void setClientiInNegozio(Map<Integer, Cliente> clientiInNegozio) {
        this.clientiInNegozio = clientiInNegozio;
    }

    public NegozioAbbigliamento (Map<Integer, Prodotto> prodottiDisponibili,
                                 Map<Integer, Cliente> clientiInNegozio,
                                 ArrayList<Ordine> elencoOrdini){
        this.prodottiDisponibili = prodottiDisponibili;
        this.clientiInNegozio = clientiInNegozio;
        this.elencoOrdini = elencoOrdini;
    }

    public void aggiungiProdotto(Prodotto prodotto){
        this.prodottiDisponibili.put(prodotto.getCodice(),prodotto);
    }

    public void rimuoviProdotto(int codiceProdotto){
        this.prodottiDisponibili.remove(codiceProdotto);
    }
    public void aggiunguCliente(Cliente cliente){
        this.clientiInNegozio.put(cliente.getCodice(),cliente);
    }
    public void rimuoviCliente(int codiceCliente){
        this.clientiInNegozio.remove(codiceCliente);
    }
    public void effettuaOrdine(Ordine ordine){
        this.elencoOrdini.add(ordine);
    }

    public void visualizzaProdottiDisponibili(){
        System.out.println("Elenco prodotti in negozio: ");
        for ( Map.Entry<Integer,Prodotto> entry : this.prodottiDisponibili.entrySet())
            System.out.println("Descrizione: "+entry.getValue().getDescrizione()+
                            " - prezzo: "+entry.getValue().getPrezzo());
        System.out.println("\n");
    }

    public void visualizzaClienti(){
        System.out.println("Elenco clienti: ");
        for ( Map.Entry<Integer,Cliente> entry : this.clientiInNegozio.entrySet())
            System.out.println("Nome cliente: "+entry.getValue().getNome());
        System.out.println("\n");
    }

    public void visualizzaOrdini(){
        System.out.println("Elenco ordini: ");
        for( int i=0; i<this.elencoOrdini.size(); i++ )
            System.out.println("Acquirente: "+elencoOrdini.get(i).getCliente().getNome()+
                            " - prodotto: "+elencoOrdini.get(i).prodotto.getTipo()+
                            " - quantita': "+elencoOrdini.get(i).prodotto.getQuantita());
        System.out.println("\n");
    }
}