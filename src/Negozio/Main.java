package Negozio;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        //CLIENTI IN NEGOZIO
        Cliente cliente1 = new Cliente(12, "Mario Rossi", 45);
        Cliente cliente2 = new Cliente(13, "Luigi Bianchi", 36);
        Cliente cliente3 = new Cliente(14, "Andrea Verdi", 27);

        //PRODOTTI IN NEGOZIO
        Prodotto prodotto1 = new Prodotto(3, TipoProdotto.MAGLIA, "Maglia rossa", 12, 5);
        Prodotto prodotto2 = new Prodotto(4, TipoProdotto.PANTALONI, "Pantaloni neri", 35, 5);
        Prodotto prodotto3 = new Prodotto(5, TipoProdotto.SCARPE, "Scarpe da tennis", 50, 5);
        Prodotto prodotto4 = new Prodotto(6, TipoProdotto.SCARPE, "Scarpe da basket", 70, 5);

        //PRODOTTI DA ACQUISTARE
        Prodotto prodotto5 = new Prodotto(3, TipoProdotto.MAGLIA, "Maglia rossa", 12, 1);
        Prodotto prodotto6 = new Prodotto(5, TipoProdotto.SCARPE, "Scarpe da tennis", 50, 1);

        //ORDINI
        Ordine ordine1 = new Ordine(cliente1, prodotto5);
        Ordine ordine2 = new Ordine(cliente2, prodotto6);

        //CREO IL NEGOZIO
        Map<Integer, Prodotto> prodottiDisponibili = new HashMap<>();
        Map<Integer, Cliente> clientiInNegozio = new HashMap<>();
        ArrayList<Ordine> elencoOrdini = new ArrayList<>();

        NegozioAbbigliamento negozioAbbigliamento = new NegozioAbbigliamento(prodottiDisponibili,
                                                                            clientiInNegozio,
                                                                            elencoOrdini);

        //RIEMPIO IL NEGOZIO
        negozioAbbigliamento.aggiunguCliente(cliente1);
        negozioAbbigliamento.aggiunguCliente(cliente2);
        negozioAbbigliamento.aggiunguCliente(cliente3);

        negozioAbbigliamento.aggiungiProdotto(prodotto1);
        negozioAbbigliamento.aggiungiProdotto(prodotto2);
        negozioAbbigliamento.aggiungiProdotto(prodotto3);
        negozioAbbigliamento.aggiungiProdotto(prodotto4);

        //STAMPO L'ELENCO DEI CLIENTI E DEI PRODOTTI IN NEGOZIO
        negozioAbbigliamento.visualizzaClienti();
        negozioAbbigliamento.visualizzaProdottiDisponibili();

        //FACCIO ACQUISTI
        negozioAbbigliamento.effettuaOrdine(ordine1);
        negozioAbbigliamento.effettuaOrdine(ordine2);

        //RIMUOVO CLIENTI E PRODOTTI
        negozioAbbigliamento.rimuoviProdotto(5);
        negozioAbbigliamento.rimuoviCliente(14);

        //STAMPO L'ELENCO DEI CLIENTI E DEI PRODOTTI IN NEGOZIO
        negozioAbbigliamento.visualizzaClienti();
        negozioAbbigliamento.visualizzaProdottiDisponibili();
    }
}