package Negozio;

public class Cliente {
    int codice;
    String nome;
    int eta;

    public int getCodice() {
        return codice;
    }

    public void setCodice(int codice) {
        this.codice = codice;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getEta() {
        return eta;
    }

    public void setEta(int eta) {
        this.eta = eta;
    }

    public Cliente(int codice, String nome, int eta){
        this.codice = codice;
        this.nome = nome;
        this.eta = eta;
    }
}
