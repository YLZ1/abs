package Negozio;

public class Ordine {
    private Cliente cliente;
    protected Prodotto prodotto;

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Prodotto getProdotto() {
        return prodotto;
    }

    public void setProdotto(Prodotto prodotto) {
        this.prodotto = prodotto;
    }

    public void setProdottoQuantita(Prodotto prodotto) {
        this.prodotto = prodotto;
    }

    public Ordine(Cliente cliente, Prodotto prodotto){
        this.cliente = cliente;
        this.prodotto = prodotto;
    }

    public void stampaOrdine(Ordine ordine){
        System.out.println("Acquirente: "+ordine.getCliente().getNome()+" - "+ordine.prodotto.getTipo()+" - "+ordine.prodotto.getQuantita());
    }
}