package Negozio;

public class Prodotto {
    private int codice;
    private TipoProdotto tipo;
    private String descrizione;
    private double prezzo;
    private int quantita;

    public int getCodice() {
        return codice;
    }

    public void setCodice(int codice) {
        this.codice = codice;
    }

    public TipoProdotto getTipo() {
        return tipo;
    }

    public void setTipo(TipoProdotto tipo) {
        this.tipo = tipo;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public double getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }

    public int getQuantita() {
        return quantita;
    }

    public void setQuantita(int quantita) {
        this.quantita = quantita;
    }

    public Prodotto(int codice, TipoProdotto tipo, String descrizione, double prezzo, int quantita){
        this.codice = codice;
        this.tipo = tipo;
        this.descrizione = descrizione;
        this.prezzo = prezzo;
        this.quantita = quantita;
    }
}