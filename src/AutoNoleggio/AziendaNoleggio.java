package AutoNoleggio;

import java.util.HashSet;
public class AziendaNoleggio {
    private HashSet<Auto> aziendaNoleggio;
    public HashSet<Auto> getAziendaNoleggio() {
        return this.aziendaNoleggio;
    }

    public AziendaNoleggio() {
        this.aziendaNoleggio = new HashSet<>();
    }
    public void aggiungiAuto(Auto auto) {
        this.aziendaNoleggio.add(auto);
    }
    public Auto cercaNumeroTarga(String numeroTarga){
            for ( Auto auto : this.aziendaNoleggio )
                if ( auto.getNumeroTarga().equals(numeroTarga) )
                    return auto;
            return null;
    }
    public void rimuoviAuto(String numeroTarga){
        Auto rem = cercaNumeroTarga(numeroTarga);
        this.aziendaNoleggio.remove(rem);
    }
    public void visualizzaAutoDisponibili(){
        System.out.println("Auto disponibili: ");
        for ( Auto auto1 : this.aziendaNoleggio )
            auto1.stampaAuto(auto1);
    }

    public double calcolaPrezzoNoleggio(int giorni, String numeroTarga){
        double res = 0;
        double g2 = (double) giorni;
        Auto nol = cercaNumeroTarga(numeroTarga);
        return res = res+g2*nol.getPrezzoXunGiorno();
    }
}