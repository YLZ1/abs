package AutoNoleggio;

public class Main {
    public static void main(String[] args) {
        AziendaNoleggio autoDisponibili = new AziendaNoleggio();

        Auto auto1 = new Auto("R G9834", "Wolkswagen", "1", 30);
        Auto auto2 = new Auto("F E4321", "Hyundai", "3", 25);
        Auto auto3 = new Auto("OS J3519", "BMW", "7", 40);

        autoDisponibili.aggiungiAuto(auto1);
        autoDisponibili.aggiungiAuto(auto2);
        autoDisponibili.aggiungiAuto(auto3);

        autoDisponibili.visualizzaAutoDisponibili();
        autoDisponibili.rimuoviAuto("R G9834");
        autoDisponibili.visualizzaAutoDisponibili();

        System.out.println("Prezzo giornaliero: "+autoDisponibili.calcolaPrezzoNoleggio(4, "OS J3519"));
    }
}