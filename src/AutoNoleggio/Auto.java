package AutoNoleggio;

public class Auto {
    private String numeroTarga;
    private String marca;
    private String modello;
    private double prezzoXunGiorno;

    public String getNumeroTarga() {
        return numeroTarga;
    }

    public void setNumeroTarga(String numeroTarga) {
        this.numeroTarga = numeroTarga;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModello() {
        return modello;
    }

    public void setModello(String modello) {
        this.modello = modello;
    }

    public double getPrezzoXunGiorno() {
        return prezzoXunGiorno;
    }

    public void setPrezzoXunGiorno(double prezzoXunGiorno) {
        this.prezzoXunGiorno = prezzoXunGiorno;
    }
    public Auto(String numeroTarga, String marca, String modello, double prezzoXunGiorno){
        this.numeroTarga = numeroTarga;
        this.marca = marca;
        this.modello = modello;
        this.prezzoXunGiorno = prezzoXunGiorno;
    }

    public void stampaAuto(Auto auto1){
        System.out.println("Targa: "+auto1.getNumeroTarga()+" - marca: "+auto1.getMarca()+" - modello: "+auto1.getModello()+" - prezzo (al giorno): "+auto1.getPrezzoXunGiorno());
    }
}
