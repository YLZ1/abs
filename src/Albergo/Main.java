package Albergo;

public class Main {
    public static void main(String[] args) {
        GestorePrenotazioni gestorePrenotazioni = new GestorePrenotazioni();

        Singola singola1 = new Singola(1,false);
        Singola singola2 = new Singola(2,false);
        Doppia doppia1 = new Doppia(3,true);
        Doppia doppia2 = new Doppia(4,false);
        Suite suite1 = new Suite(5, true);
        Suite suite2 = new Suite(6, false);

        gestorePrenotazioni.aggiungiCamera(singola1);
        gestorePrenotazioni.aggiungiCamera(singola2);
        gestorePrenotazioni.aggiungiCamera(doppia2);
        gestorePrenotazioni.aggiungiCamera(suite2);

        gestorePrenotazioni.visualizzaCamereDisponibili();

        gestorePrenotazioni.aggiornaStatoPrenotazione(1, true);
        gestorePrenotazioni.aggiornaStatoPrenotazione(2, true);
        gestorePrenotazioni.aggiornaStatoPrenotazione(3, false);
        gestorePrenotazioni.visualizzaCamereDisponibili();
    }
}
