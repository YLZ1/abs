package Albergo;

import java.util.HashMap;
import java.util.Map;

public class GestorePrenotazioni {
    Map<Integer, Camera> gestorePrenotazioni;

    public Map<Integer, Camera> getGestorePrenotazioni() {
        return gestorePrenotazioni;
    }

    public GestorePrenotazioni(){
        this.gestorePrenotazioni = new HashMap<>();
    }

    public void aggiungiCamera(Camera camera){
        this.gestorePrenotazioni.put(camera.getNumeroCamera(), camera);
    }

    public void rimuoviCamera(int numeroCamera){
        this.gestorePrenotazioni.remove(numeroCamera);
    }

    public Camera cercaCamera(int numeroCamera){
        if( this.gestorePrenotazioni.containsKey(numeroCamera) ){
            return this.gestorePrenotazioni.get(numeroCamera);
        }
        return null;
    }

    public void aggiornaStatoPrenotazione(int numeroCamera, boolean prenotata){
        if ( prenotata ) {
            aggiungiCamera(cercaCamera(numeroCamera));
            //this.gestorePrenotazioni.remove(numeroCamera);
        }
        else {
            rimuoviCamera(numeroCamera);
            //this.gestorePrenotazioni.put(numeroCamera, this.gestorePrenotazioni.get(numeroCamera));
        }
    }
    public void visualizzaCamereDisponibili(){
        for ( Map.Entry<Integer,Camera> entry : this.gestorePrenotazioni.entrySet()) {
            System.out.println(entry.getKey());
        }
    }
}
