package Albergo;

public abstract class Camera {
    protected int numeroCamera;
    protected boolean statoPrenotazione;       //true se prenotata, false se non prenotata

    public int getNumeroCamera() {
        return numeroCamera;
    }

    public void setNumeroCamera(int numeroCamera) {
        this.numeroCamera = numeroCamera;
    }

    public boolean isStatoPrenotazione() {
        return statoPrenotazione;
    }

    public void setStatoPrenotazione(boolean statoPrenotazione) {
        this.statoPrenotazione = statoPrenotazione;
    }

    public Camera(int numeroCamera, boolean statoPrenotazione){
        this.numeroCamera = numeroCamera;
        this.statoPrenotazione = statoPrenotazione;
    }
}