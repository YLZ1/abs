import java.util.Scanner;

public class Libro {
    private String titolo;
    private int isbn;
    private String autore;
    private int anno;

    public String getTitolo(){
        return this.titolo;
    }
    public int getISBN(){
        return this.isbn;
    }
    public String getAutore(){
        return this.autore;
    }
    public int getAnno(){
        return this.anno;
    }
    public void setTitolo(String titolo){
        this.titolo = titolo;
    }
    public  void setIsbn(int isbn){
        this.isbn = isbn;
    }
    public void setAutore(String autore){
        this.autore = autore;
    }
    public  void setAnno(int anno){
        this.anno = anno;
    }
    public static void stampaLibro(Libro libro){
        System.out.println("Titolo: "+libro.getTitolo());
        System.out.println("ISBN: "+libro.getISBN());
        System.out.println("Autore: "+libro.getAutore());
        System.out.println("Anno: "+libro.getAnno());
    }
    public static void ottieniInfo(String titolo, int isbn, String autore, int anno){
        Libro libro1 = new Libro();
        libro1.setTitolo(titolo);
        libro1.setIsbn(isbn);
        libro1.setAutore(autore);
        libro1.setAnno(anno);
        stampaLibro(libro1);
    }

    public static void main(String[] args) {
        System.out.println("Digitare il titolo: ");
        Scanner tit = new Scanner(System.in);
        String titolo = tit.nextLine();
        System.out.println("Digitare il codice ISBN: ");
        Scanner is = new Scanner(System.in);
        int isbn = is.nextInt();
        System.out.println("Digitare il nome dell'autore: ");
        Scanner au = new Scanner(System.in);
        String autore = au.nextLine();
        System.out.println("Digitare l'anno di uscita: ");
        Scanner an = new Scanner(System.in);
        int anno = an.nextInt();

        ottieniInfo( titolo, isbn, autore, anno );
    }
}