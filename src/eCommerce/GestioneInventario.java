package eCommerce;

import java.util.HashMap;

public class GestioneInventario {
    private HashMap<Prodotto, Integer> prodotti;

    public HashMap<Prodotto, Integer> getProdotti() {
        return prodotti;
    }

    public void setProdotti(HashMap<Prodotto, Integer> prodotti) {
        this.prodotti = prodotti;
    }

    public GestioneInventario(HashMap<Prodotto, Integer> prodotti) {
        this.prodotti = prodotti;
    }

    public void aggiornaQuantita(Prodotto prodotto, int quantita){
        this.prodotti.put(prodotto,quantita);
    }

    public boolean verificaDisponibilita(Prodotto prodotto){
        if( this.prodotti.containsKey(prodotto) )
            return true;
        return false;
    }

    public void gestisciRifornimento(){     //Se la quantita' di un prodotto e' 0, ordina 10 di quei prodotti
        for (HashMap.Entry<Prodotto, Integer> set :
                this.prodotti.entrySet()) {
            if( set.getValue() == 0 )
                this.aggiornaQuantita(set.getKey(),10);
        }
    }
}
