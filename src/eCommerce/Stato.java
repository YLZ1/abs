package eCommerce;

public enum Stato {
    IN_ATTESA,
    SPEDITO,
    CONSEGNATO;
}
