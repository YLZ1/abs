package eCommerce;

public class Prodotto {
    private String id;
    private String nome;
    private double prezzo;
    private int quantitaInventario;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }

    public int getQuantitaInventario() {
        return quantitaInventario;
    }

    public void setQuantitaInventario(int quantitaInventario) {
        this.quantitaInventario = quantitaInventario;
    }

    public Prodotto(String id, String nome, double prezzo, int quantitaInventario){
        this.id = id;
        this.nome = nome;
        this.prezzo = prezzo;
        this.quantitaInventario = quantitaInventario;
    }

    public boolean disponibileDisponibile(Prodotto prodotto){
        if ( prodotto.getQuantitaInventario() > 0 )
            return true;
        return false;
    }
}