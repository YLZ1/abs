package hikouki;

public class MainH {
    public static void main(String[] args) {
        Passeggero passeggero1 = new Passeggero("Mario Rossi", "Italia", "AZ204", "16F", "vegetariano");
        Passeggero passeggero2 = new Passeggero("Moritz Schneider", "Germania", "AZ204", "15F", "standard");
        Passeggero passeggero3 = new Passeggero("Naoki Fujimoto", "Giappone", "NH512", "34A", "vegetariano");

        Aereoporto partenza = new Aereoporto("Linate", "Milano", "LIN");
        Aereoporto arrivo = new Aereoporto("Franz Josef Strauss", "Monaco", "FJS");
        Aereoporto mezzo = new Aereoporto("Haneda", "Tokyo", "HND");

        Volo volo1 = new Volo("AZ204", partenza, arrivo, "Airbus 300", 120, 0);

        VoloNonDiretto volo2 = new VoloNonDiretto("AZ204", partenza, arrivo, "Airbus 300", 120, 0, 0, 10);
        VoloNonDiretto.aggiungiScalo(volo2, mezzo);

        Volo.aggiungiPasseggero(passeggero1,volo2);
        Volo.aggiungiPasseggero(passeggero2,volo2);
        Volo.aggiungiPasseggero(passeggero3,volo2);

        Passeggero.cambiaPosto(passeggero3, "56D");

        System.out.println("Descrizione del volo:");
        //System.out.println(Volo.descriviVolo(volo1));
        System.out.println(VoloNonDiretto.descriviVoloNonDiretto(volo2));

        System.out.println("Elenco Passeggeri:");
        for ( int i=0; i<volo2.getNumpass(); i++ )
            System.out.println(Volo.elencoP(volo2)[i]);

        System.out.println("Elenco dei posti dei passeggeri con pasto vegetariano:");
        for ( int i=0; i<Volo.elencoVegP(volo2).length; i++ )
            System.out.println(Volo.elencoVegP(volo2)[i]);
    }
}