package hikouki;
public class Volo{
    private String sigla;
    private Aereoporto partenza;
    private Aereoporto arrivo;
    private String aereomobile;
    private int maxpass;        //massimo numero passeggeri nel volo
    private int numpass;        //numero passeggeri nel volo
    private Passeggero[] listapasseggeri;
    public String getSigla() {
        return sigla;
    }
    public void setSigla(String sigla) {
        this.sigla = sigla;
    }
    public Aereoporto getPartenza() {
        return partenza;
    }
    public void setPartenza(Aereoporto partenza) {
        this.partenza = partenza;
    }
    public Aereoporto getArrivo() {
        return arrivo;
    }
    public void setArrivo(Aereoporto arrivo) {
        this.arrivo = arrivo;
    }
    public String getAereomobile() {
        return aereomobile;
    }
    public void setAereomobile(String aereomobile) {
        this.aereomobile = aereomobile;
    }
    public Passeggero[] getListapasseggeri() {
        return listapasseggeri;
    }
    public void setListapasseggeri(Passeggero[] listapasseggeri) {
        this.listapasseggeri = listapasseggeri;
    }
    public int getMaxpass() {
        return maxpass;
    }
    public void setMaxpass(int maxpass) {
        this.maxpass = maxpass;
    }
    public int getNumpass() {
        return numpass;
    }
    public void setNumpass(int numpass) {
        this.numpass = numpass;
    }
    public Volo(String sigla, Aereoporto partenza, Aereoporto arrivo, String aereomobile, int maxpass, int numpass){
        this.sigla = sigla;
        this.partenza = partenza;
        this.arrivo = arrivo;
        this.aereomobile = aereomobile;
        this.maxpass = maxpass;
        this.numpass = numpass;
        Passeggero[] listapasseggeri = new Passeggero[maxpass];
        this.listapasseggeri = listapasseggeri;
    }
    public static int aggiungiPasseggero(Passeggero passeggero1, Volo volo1){
        if ( volo1.getNumpass()+1 >= volo1.getMaxpass() || volo1.getSigla() != passeggero1.getSiglavolo() )
            return -1;
        else{
            volo1.getListapasseggeri()[volo1.getNumpass()] = new Passeggero(passeggero1.getNome(),passeggero1.getNazionalita(),passeggero1.getSiglavolo(),passeggero1.getPosto(),passeggero1.getPasto());
            volo1.setNumpass(volo1.getNumpass()+1);
            return 1;
        }
    }
    public static String descriviVolo(Volo volo1){
        String res = new String();
        res = "Volo "+ volo1.getSigla()+" "+volo1.getPartenza().getNome()+" "+volo1.getPartenza().getCitta()+" - "+volo1.getArrivo().getNome()+" "+volo1.getArrivo().getCitta();
        return res;
    }
    public static String[] elencoP(Volo volo1){
        String[] elenco;
        elenco = new String[volo1.getNumpass()];
        for ( int i=0; i<volo1.getNumpass(); i++ )
            elenco[i] = volo1.getListapasseggeri()[i].getNome();
        return elenco;
    }
    public static String[] elencoVegP(Volo volo1){
        String[] elenco;
        int count = 0;
        for ( int i=0; i<volo1.getNumpass(); i++ )
            if ( volo1.getListapasseggeri()[i].getPasto().equals("vegetariano") == true )
                count++;
        elenco = new String[count];
        count = 0;
        for ( int i=0; i<volo1.getNumpass(); i++ )
            if ( volo1.getListapasseggeri()[i].getPasto().equals("vegetariano") == true ){
                elenco[count] = volo1.getListapasseggeri()[i].getPosto();
                count++;
            }
        return elenco;
    }
}