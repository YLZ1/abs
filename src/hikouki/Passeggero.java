package hikouki;

public class Passeggero {
    private String nome;
    private String nazionalita;
    private String siglavolo;
    private String posto;
    private String pasto;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNazionalita() {
        return nazionalita;
    }

    public void setNazionalita(String nazionalita) {
        this.nazionalita = nazionalita;
    }

    public String getSiglavolo() {
        return siglavolo;
    }

    public void setSiglavolo(String siglavolo) {
        this.siglavolo = siglavolo;
    }

    public String getPosto() {
        return posto;
    }

    public void setPosto(String posto) {
        this.posto = posto;
    }

    public String getPasto() {
        return pasto;
    }

    public void setPasto(String pasto) {
        this.pasto = pasto;
    }

    public Passeggero(String nome, String nazionalita, String siglavolo, String posto, String pasto){
        this.nome = nome;
        this.nazionalita = nazionalita;
        this.siglavolo = siglavolo;
        this.posto = posto;
        this.pasto = pasto;
    }

    public static void cambiaPosto(Passeggero passeggero1, String posto){
        passeggero1.setPosto(posto);
    }
}
