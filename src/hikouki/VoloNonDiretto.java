package hikouki;

public class VoloNonDiretto extends Volo{
    private int nScali;
    private int nMaxScali;
    private Aereoporto[] scalo;
    public int getnScali() {
        return nScali;
    }
    public void setnScali(int nScali) {
        this.nScali = nScali;
    }
    public int getnMaxScali() {
        return nMaxScali;
    }
    public void setnMaxScali(int nMaxScali) {
        this.nMaxScali = nMaxScali;
    }
    public Aereoporto[] getScalo() {
        return scalo;
    }
    public void setScalo(Aereoporto[] scalo) {
        this.scalo = scalo;
    }
    public VoloNonDiretto(String sigla, Aereoporto partenza, Aereoporto arrivo, String aereomobile, int maxpass, int numpass, int nScali, int nMaxScali){
        super(sigla, partenza, arrivo, aereomobile, maxpass, numpass);
        this.nScali = nScali;
        this.nMaxScali = nMaxScali;
        Aereoporto[] scalo = new Aereoporto[nMaxScali];
        this.scalo = scalo;
    }
    public static int aggiungiScalo(VoloNonDiretto volo1, Aereoporto scalo){
        if ( volo1.getnScali()+1 > volo1.getnMaxScali() )
            return -1;
        else {
            volo1.getScalo()[volo1.getnScali()] = new Aereoporto(scalo.getNome(), scalo.getCitta(), scalo.getSigla());
            volo1.setnScali(volo1.getnScali()+1);
            return 1;
        }
    }
    public static String descriviVoloNonDiretto(VoloNonDiretto volo1){
        String res = new String();
        res = "Volo "+ volo1.getSigla()+" "+volo1.getPartenza().getNome()+" "+volo1.getPartenza().getCitta()+" - "+volo1.getArrivo().getNome()+" "+volo1.getArrivo().getCitta();
        if ( volo1.getnScali() >= 1 ) {
            res = res + " via";
            for (int i = 0; i < volo1.getnScali(); i++) {
                res = res + " - "+volo1.getScalo()[i].getNome()+" "+volo1.getScalo()[i].getCitta();
            }
        }
        return res;
    }
}