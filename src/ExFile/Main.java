package ExFile;

import java.io.File;
import java.io.FileWriter;
import java.io.FileReader;
import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        /*try {
            File input = new File("input.txt");
            if (input.createNewFile()) {
                System.out.println("File creato: " + input.getName());
            } else {
                System.out.println("File gia' esistente.");
            }
        } catch (Exception e) {
            System.out.println("Errore nella creazione del file.");
        }*/

        try {
            FileWriter input = new FileWriter("input.txt");
            input.write("1,2,3,4,5\n6,7,8,9,10\n11,12,13,14,15");
            input.close();
            System.out.println("Esito della scrittura su file: positivo.");
        } catch (Exception e) {
            System.out.println("Errore nella scrittura del file.");
        }

        int[] array = new int[0];
        try {
            FileReader input = new FileReader("input.txt");
            Scanner myReader = new Scanner(input);
            int i = 0;
            int k=0;
            array = new int[3];
            array[0] = 0;
            array[1] = 0;
            array[3] = 0;
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                for ( int j=0; j<data.length(); j++ ){
                    if ( data.charAt(j) == ',' ) {
                        array[i] += Integer.parseInt(data.substring(k, j - 1));
                        k = j;
                    }
                }
                i++;
            }
            System.out.println("Esito della lettura su file: positivo");
        } catch (Exception e) {
            System.out.println("Errore nella lettura del file");
        }

        String l1 = String.valueOf(array[0]);
        String l2 = String.valueOf(array[1]);
        String l3 = String.valueOf(array[2]);

        try {
            FileWriter output = new FileWriter("output.txt");
            output.write(l1 + "\n" + l2 + "\n" + l3);
            output.close();
            System.out.println("Esito della scrittura su file: positivo.");
        } catch (Exception e) {
            System.out.println("Errore nella scrittura del file.");
        }
    }
}