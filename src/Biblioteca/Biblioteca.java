package Biblioteca;

import java.util.ArrayList;
import java.util.List;

public class Biblioteca {
    public static void aggiungiLibro(List<Libro> listaLibri, Libro libro1){
        listaLibri.add(libro1);
    }
    public static int ricercaLibro(List<Libro> listaLibri, Libro libro1){
        return listaLibri.indexOf(libro1);
    }
    public static void stampaLista(List<Libro> listaLibri){
        System.out.println("Lista libri:");
        for ( int i=0; i<listaLibri.size(); i++ )
            listaLibri.get(i).stampaLibro(listaLibri.get(i));
        System.out.println("\n");
    }
    public static int numeroPagine(List<Libro> listaLibro){
        int res = 0;

        for ( int i=0; i<listaLibro.size(); i++ )
             {
                if (listaLibro.get(i) instanceof LibroStampato)
                    res += ((LibroStampato) listaLibro.get(i)).getnPagine();
            }
        return res;
    }

    public static void main(String[] args) {
        List<Libro> listaLibro = new ArrayList<>();

        LibroStampato libro1 = new LibroStampato("Il vecchio e il mare", "Ernest Hemingway", 1952, 120);
        LibroStampato libro2 = new LibroStampato("Anna Karenina", "Lev N. Tolstoj", 1878, 700);
        LibroStampato libro3 = new LibroStampato("Kokoro", "Natsume Souseki", 1914, 250);
        LibroStampato libro4 = new LibroStampato("Noruwei no mori", "Haruki Murakami", 1987, 450);

        LibroDigitale libro5 = new LibroDigitale("Linguaggio C", "Kernighan-Ritchie", 1978, "PDF");
        LibroDigitale libro6 = new LibroDigitale("Categories for the Working Mathematician", "Eilenberg-MacLane", 1997, "PDF");

        aggiungiLibro(listaLibro, libro1);
        aggiungiLibro(listaLibro, libro2);
        aggiungiLibro(listaLibro, libro3);
        aggiungiLibro(listaLibro, libro4);
        aggiungiLibro(listaLibro, libro5);
        aggiungiLibro(listaLibro, libro6);

        stampaLista(listaLibro);
        System.out.println("Il libro Kokoro e' in posizione: "+ricercaLibro(listaLibro, libro3)+"\n");
        System.out.println("Il numero totale di pagine dei libri stampati e': "+numeroPagine(listaLibro));
    }
}