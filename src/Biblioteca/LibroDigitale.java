package Biblioteca;

public class LibroDigitale extends Libro{
    private String formatoDig;
    public String getFormatoDig() {
        return formatoDig;
    }
    public void setFormatoDig(String formatoDig) {
        this.formatoDig = formatoDig;
    }
    public LibroDigitale(String titolo, String autore, int anno, String formatoDig){
        super(titolo, autore, anno);
        this.formatoDig = formatoDig;
    }
}