import java.util.Scanner;

/**
 * @author ylz
 */
public class ExerciseIV {
    public static void main(String[] args) {
        System.out.println("Digitare un carattere: ");
        Scanner cc = new Scanner(System.in);
        char c = cc.next().charAt(0);
        System.out.println("Digitare una stringa: ");
        Scanner ss = new Scanner(System.in);
        String s = ss.nextLine();
        System.out.println("Il carattere "+c+ " e' presente nella stringa "+s+": "+instr(c, s));
        System.out.println("Il carattere "+c+" e' presente nella stringa "+s+" con frequenza: "+countstr(c, s));
        System.out.println("La prima posizione del carattere "+c+" nella stringa "+s+" e': "+npos(c, s));
    }
    public static boolean instr(char c, String s){      //Restituisce true se c appartiene a s, false altrimenti
        c = Character.toLowerCase(c);
        s=s.toLowerCase();
        for ( int i=0; i<s.length(); i++ )
            if (s.charAt(i) == c)
                return true;
        return false;
    }
    public static int countstr(char c, String s){       //Restituisce il numero di occorrenze di c in s
        c = Character.toLowerCase(c);
        s=s.toLowerCase();
        int count = 0;
        for ( int i=0; i<s.length(); i++ )
            if (s.charAt(i) == c)
                count++;
        return count;
    }
    public static int npos(char c, String s){      //Restituisce la prima posizione di c appartiene in s, -1 se c non appartiene a s
        c = Character.toLowerCase(c);
        s=s.toLowerCase();
        for ( int i=0; i<s.length(); i++ )
            if (s.charAt(i) == c)
                return i;
        return -1;
    }
}