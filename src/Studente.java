import java.util.Scanner;

public class Studente {
    private String nome;
    private String cognome;
    private int matricola;
    public String getNome(){
        return this.nome;
    }
    public String getCognome(){
        return this.cognome;
    }
    public int getMatricola(){
        return this.matricola;
    }
    /*public void setStudente(String nome, String cognome, int num_matricola){
        this.nome = nome;
        this.cognome = cognome;
        this.num_matricola = num_matricola;
    }*/
    public Studente(){              //Costruttore vuoto da usare
    }
    public void setNome(String nome){
        this.nome = nome;
    }
    public void setCognome(String cognome){
        this.cognome = cognome;
    }
    public void setMatricola(int matricola){
        this.matricola = matricola;
    }

    /*public Studente(String nome, String cognome, int matricola){
        this.nome = nome;
        this.cognome = cognome;
        this.matricola = matricola;
    }*/

    public static void stampaInfo(Studente studente1){
        System.out.println("Nome: "+studente1.getNome());
        System.out.println("Cognome: "+studente1.getCognome());
        System.out.println("Numero di matricola: "+studente1.getMatricola());
    }

    /*public static void main(String[] args) {
        System.out.println("Digitare un nome: ");
        Scanner nm = new Scanner(System.in);
        String nome = nm.nextLine();
        System.out.println("Digitare una cognome: ");
        Scanner co = new Scanner(System.in);
        String cognome = co.nextLine();
        System.out.println("Digitare un numero di matricola: ");
        Scanner in = new Scanner(System.in);
        int matricola = in.nextInt();

        Studente studente1 = new Studente();
        studente1.setNome(nome);
        studente1.setCognome(cognome);
        studente1.setMatricola(matricola);

        //studente1.setStudente(nome, cognome,num_matricola);

        //Studente studente1 = new Studente(nome, cognome, num_matricola);
        System.out.println("Nome dello studete: "+studente1.getNome());
        System.out.println("Cognome dello studete: "+studente1.getCognome());
        System.out.println("Numero di matricola dello studete: "+studente1.getMatricola());
    }*/
}