public class Banca {
    private int codice;
    private int saldo;
    public int getCodice(){
        return this.codice;
    }
    public int getSaldo(){
        return this.saldo;
    }
    public void setCodice(int codice){
        this.codice = codice;
    }
    public void setSaldo(int saldo){
        this.saldo = saldo;
    }
    public static int modSaldo(int operazione, Banca banca1){
        return banca1.getSaldo() + operazione;
    }
    public static void stampaSaldo(Banca banca1){
        System.out.println("Il conto n."+banca1.getCodice()+" ha un saldo di "+banca1.getSaldo());
    }

    public static void main(String[] args) {
        Banca banca1 = new Banca();
        banca1.setCodice(1234);
        banca1.setSaldo(0);
        stampaSaldo(banca1);
        banca1.setSaldo(modSaldo( 1000, banca1));
        banca1.setSaldo(modSaldo( -500, banca1));
        banca1.setSaldo(modSaldo( -400, banca1));
        stampaSaldo(banca1);
    }
}