package Palestra;
/**
 * @author Lorenzo
 * @version 1.0
 * Classe che estende Exceptpion. Stampa un messaggio di errore nel caso in cui un abbonamamento sia scaduto
 */
public class AbbonamentoNonValidoException extends Exception{
    public AbbonamentoNonValidoException(){}
    public AbbonamentoNonValidoException(String message){
        super(message);
    }
}
