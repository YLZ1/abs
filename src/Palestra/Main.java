package Palestra;

import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) throws ClassePienaException, AbbonamentoNonValidoException, MembroNonIscrittoAllaClasse{

        //Definizione e inizializzazione della palestra
        HashMap<String,TipoAbbonamento> abbonamentoMembri = new HashMap<>();
        ArrayList<ClasseDiAllenamento> listaClassiDiAllenamento = new ArrayList<>();
        GestionePalestra gestionePalestra = new GestionePalestra(abbonamentoMembri, listaClassiDiAllenamento);

        //Definizione e inizializzazione membri
        Membro membro1 = new Membro("Mario", "Rossi", "123", TipoAbbonamento.giornaliero);
        Membro membro2 = new Membro("Luigi", "Bianchi", "125", TipoAbbonamento.mensile);
        Membro membro3 = new Membro("Andrea", "Verdi", "126", TipoAbbonamento.scaduto);
        Membro membro4 = new Membro("Guido", "Mista", "127", TipoAbbonamento.annuale);


        //Definizione e inizializzazione istruttori
        Istruttore istruttore1 = new Istruttore("Jinrou", "Liu", "845A", TipoAbbonamento.istruttore, "Baguazhang", 40);
        Istruttore istruttore2 = new Istruttore("Robert Jay", "Arnold", "845B", TipoAbbonamento.istruttore, "Baguazhang", 20);

        //Definizione e inizializzazione di classi di allenamento
        ArrayList<Membro> listaMembriIscritti1 = new ArrayList<>();
        ArrayList<Membro> listaMembriIscritti2 = new ArrayList<>();
        HashMap<String,Float> listaVoti1 = new HashMap<>();
        HashMap<String,Float> listaVoti2 = new HashMap<>();
        ClasseDiAllenamento classeDiAllenamento1 = new ClasseDiAllenamento("Baguazhang", "Mercoledi' 18:00-20:00",listaMembriIscritti1, istruttore1, 2, listaVoti1);
        ClasseDiAllenamento classeDiAllenamento2 = new ClasseDiAllenamento("Qigong", "Lunedi'' 18:00-20:00",listaMembriIscritti2, istruttore2, 2, listaVoti2);

        //Registro i membri in palestra
        gestionePalestra.registrazioneNuoviMembri(membro1);
        gestionePalestra.registrazioneNuoviMembri(membro2);
        gestionePalestra.registrazioneNuoviMembri(membro3);
        gestionePalestra.registrazioneNuoviMembri(membro4);

        //Aggiungo le classi di allenamento in palestra
        gestionePalestra.aggiungiClasseDiAllenamento(classeDiAllenamento1);
        gestionePalestra.aggiungiClasseDiAllenamento(classeDiAllenamento2);

        //Cambio istruttore alla classe 1
        gestionePalestra.assegnaIstruttore(classeDiAllenamento1, istruttore2);

        //Iscrivo i membri 1 e 2 alla classe 1
        gestionePalestra.iscrizioneMembriNelleCLassi(classeDiAllenamento1, membro1);
        gestionePalestra.iscrizioneMembriNelleCLassi(classeDiAllenamento1, membro2);
        //gestionePalestra.iscrizioneMembriNelleCLassi(classeDiAllenamento1, membro4);    //Eccezione: capacita' massima raggiunta
        //gestionePalestra.iscrizioneMembriNelleCLassi(classeDiAllenamento2, membro3);  //Eccezione: il membro 3 ha l'abbonamento scaduto

        //Stampo la lista dei programmi di lunedi'
        gestionePalestra.stampaProgrammiPerGiorno("Lunedi'");
        gestionePalestra.stampaProgrammiPerGiorno("Mercoledi'");

        //I membri 1 e 2 valutano la classe 1. La media delle valutazioni viene stampata
        classeDiAllenamento1.membroVotaClasse(membro1,9);
        classeDiAllenamento1.membroVotaClasse(membro2,8);
        //classeDiAllenamento2.membroVotaClasse(membro2,8);     //Eccezione: il membro 2 non e' iscritto alla classe 2
        gestionePalestra.mediaClasse(classeDiAllenamento1);     //testo il calcolo e la stampa media classe
    }
}