package Palestra;
/**
 * @author Lorenzo
 * @version 1.0
 * La classe Istruttore estende la classe di Membro e rappresenta gli istruttori della palestra
 */

public class Istruttore extends Membro{
    private String specializzazione;
    private int anniEsperienza;

    /**
     * Metodo che restituisce la specializzazione
     * @return specializzazione
     */
    public String getSpecializzazione() {
        return specializzazione;
    }

    /**
     * Metodo che setta la specializzazione
     * @param specializzazione
     */
    public void setSpecializzazione(String specializzazione) {
        this.specializzazione = specializzazione;
    }

    /**
     * Metodo che restituisce anniEsperienza
     * @return anniEsperienza
     */
    public int getAnniEsperienza() {
        return anniEsperienza;
    }
    /**
     * Metodo che sette anniEsperienza
     * @param anniEsperienza
     */
    public void setAnniEsperienza(int anniEsperienza) {
        this.anniEsperienza = anniEsperienza;
    }
    /**
     * Metodo che costruisce Istruttore
     * @param nome
     * @param cognome
     * @param id
     * @param tipoAbbonamento
     * @param specializzazione
     * @param anniEsperienza
     */
    public Istruttore( String nome, String cognome, String id, TipoAbbonamento tipoAbbonamento, String specializzazione, int anniEsperienza){
        super(nome,cognome,id,tipoAbbonamento);
        this.specializzazione = specializzazione;
        this.anniEsperienza = anniEsperienza;
    }
}