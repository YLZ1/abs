package Palestra;
/**
 * @author Lorenzo
 * @version 1.0
 * Classe che estende Exceptpion. Stampa un messaggio di errore nel caso in cui un membro non sia iscritto alla classe
 */
public class MembroNonIscrittoAllaClasse extends Exception{
    public MembroNonIscrittoAllaClasse(){}

    public MembroNonIscrittoAllaClasse(String message){
        super(message);
    }
}