package Palestra;

/**
 * @author Lorenzo
 * @version 1.0
 * La classe Membro rappresenta gli utenti della palestra
 */

public class Membro {
    private String nome;
    private String cognome;
    private String id;
    private TipoAbbonamento tipoAbbonamento;
    /**
     * Metodo che restituisce il nome
     * @return nome
     */
    public String getNome() {
        return nome;
    }
    /**
     * Metodo che setta il nome
     * @param nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }
    /**
     * Metodo che restituisce il cognome
     * @return cognome
     */
    public String getCognome() {
        return cognome;
    }
    /**
     * Metodo che setta il cognome
     * @param cognome
     */
    public void setCognome(String cognome) {
        this.cognome = cognome;
    }
    /**
     * Metodo che restituisce l'id
     * @return id
     */
    public String getId() {
        return id;
    }
    /**
     * Metodo che setta l'id
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Metodo che restituisce il tipoAbbonamento
     * @return tipoAbbonamento
     */
    public TipoAbbonamento getTipoAbbonamento() {
        return tipoAbbonamento;
    }
    /**
     * Metodo che setta il tipoAbbonamento
     * @param tipoAbbonamento
     */
    public void setTipoAbbonamento(TipoAbbonamento tipoAbbonamento) {
        this.tipoAbbonamento = tipoAbbonamento;
    }
    /**
     *  Metodo che costruisce un membro
     *  @param nome
     *  @param cognome
     *  @param id
     *  @param tipoAbbonamento
     */
    public Membro(String nome, String cognome, String id, TipoAbbonamento tipoAbbonamento){
        this.nome = nome;
        this.cognome = cognome;
        this.id = id;
        this.tipoAbbonamento = tipoAbbonamento;
    }
    /**
     * Metodo che stampa un membro
     * @param membro
     */
    public void stampaMembro(Membro membro){
        System.out.println("Nome: "+membro.getNome()+"\t"+membro.getCognome()+"\t"+membro.getId()+"\t"+membro.getTipoAbbonamento());
    }
}