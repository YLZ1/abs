package Palestra;

/**
 * @author Lorenzo
 * @version 1.0
 * Enum che rappresenta il tipo abbonamento
 */
enum TipoAbbonamento {
    giornaliero,
    mensile,
    annuale,
    scaduto,
    istruttore
}
