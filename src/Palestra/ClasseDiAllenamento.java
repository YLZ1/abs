package Palestra;

/**
 * @author Lorenzo
 * @version 1.0
 * Classe che rappresenta una classe di allemento (i.e. un corso attivo della palestra)
 */

import java.util.ArrayList;
import java.util.HashMap;

public class ClasseDiAllenamento {
    private String nomeDellaClasse;
    private String orario;
    private ArrayList<Membro> listaMembriIscritti;
    private HashMap<String, Float> listaVoti;
    private Istruttore istruttoreAssegnato;
    private int capacita;

    /**
     * Metodo che restituisce la listaVoti (i.e. un hashmap in cui vengono segnate le valutazioni degli utenti come valore e le cui key sono gli id dei membri)
     * @return listaVoti
     */

    public HashMap<String, Float> getListaVoti() {
        return listaVoti;
    }

    /**
     * Metodo che setta la listaVoti (i.e. un hashmap in cui vengono segnate le valutazioni degli utenti come valore e le cui key sono gli id dei membri)
     * @param listaVoti
     */
    public void setListaVoti(HashMap<String, Float> listaVoti) {
        this.listaVoti = listaVoti;
    }

    /**
     * Metodo che restituisce il nomeDellaClasse
     * @return nomeDellaClasse
     */

    public String getNomeDellaClasse() {
        return nomeDellaClasse;
    }

    /**
     * Metodo che sette il nomeDellaClasse
     * @param nomeDellaClasse
     */

    public void setNomeDellaClasse(String nomeDellaClasse) {
        this.nomeDellaClasse = nomeDellaClasse;
    }

    /**
     * Metodo che restituisce l'orario
     * @return orario
     */

    public String getOrario() {
        return orario;
    }

    /**
     * Metodo che setta l'orario
     * @param orario
     */

    public void setOrario(String orario) {
        this.orario = orario;
    }

    /**
     * Metodo che restituisce la listaMembriIscritti
     * @return listaMembriIscritti
     */

    public ArrayList<Membro> getListaMembriIscritti() {
        return listaMembriIscritti;
    }

    /**
     * Metodo che setta la listaMembriIscritti
     * @param listaMembriIscritti
     */

    public void setListaMembriIscritti(ArrayList<Membro> listaMembriIscritti) {
        this.listaMembriIscritti = listaMembriIscritti;
    }

    /**
     * Metodo che restituisce l'istruttoreAssegnato
     * @return istruttoreAssegnato
     */

    public Istruttore getIstruttoreAssegnato() {
        return istruttoreAssegnato;
    }

    /**
     * Metodo che setta l'istruttoreAssegnato
     * @param istruttoreAssegnato
     */

    public void setIstruttoreAssegnato(Istruttore istruttoreAssegnato) {
        this.istruttoreAssegnato = istruttoreAssegnato;
    }

    /**
     * Metodo che restituisce la capacita'
     * @return capacita
     */

    public int getCapacita() {
        return capacita;
    }

    /**
     * Metodo che setta la capacita'
     * @param capacita
     */

    public void setCapacita(int capacita) {
        this.capacita = capacita;
    }

    /**
     * Metodo che costruisce una ClasseDiAllenamento
     * @param nomeDellaClasse
     * @param orario
     * @param listaMembriIscritti
     * @param istruttoreAssegnato
     * @param capacita
     * @param listaVoti
     */

    public ClasseDiAllenamento(String nomeDellaClasse, String orario, ArrayList<Membro> listaMembriIscritti, Istruttore istruttoreAssegnato, int capacita, HashMap<String,Float> listaVoti){
        this.nomeDellaClasse = nomeDellaClasse;
        this.orario = orario;
        this.listaMembriIscritti = listaMembriIscritti;
        this.istruttoreAssegnato = istruttoreAssegnato;
        this.capacita = capacita;
        this.listaVoti = listaVoti;
    }

    /**
     * Metodo che registra un nuovo membro
     * @param membro
     */

    public void aggiungiMembro(Membro membro){
        this.listaMembriIscritti.add(membro);
    }

    /**
     * Metodo che stampa le istruzioni di una classe di allenamento: il nome, l'orario, il nome dell'istruttore e i posti disponibili
     */

    public void stampaClasse(){
        String postiDisponibili = Integer.toString(this.getCapacita()-this.getListaMembriIscritti().size());
        System.out.println("Nome della classe: "+this.getNomeDellaClasse()+"; orario: "+this.getOrario()+"; istruttore: "+this.getIstruttoreAssegnato().getNome()+" "+this.getIstruttoreAssegnato().getCognome()+"; posti disponibili: "+postiDisponibili);
    }

    /**
     * Metodo che assegna il voto di un membro ad una classe di allenamento. Nel caso in cui il membro non sia iscritto ad una classe, viene stampato un messaggio di errore tramite la classe Exception
     * @param membro
     * @param voto
     * @throws MembroNonIscrittoAllaClasse
     */

    public void membroVotaClasse(Membro membro, float voto) throws MembroNonIscrittoAllaClasse{
        if ( !this.getListaMembriIscritti().contains(membro) ){
            throw new MembroNonIscrittoAllaClasse("L'utente "+membro.getNome()+" "+membro.getCognome()+" non e' iscritto alla classe "+this.getNomeDellaClasse());
        }
        this.listaVoti.put(membro.getId(),voto);
    }
}