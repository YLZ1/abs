package Palestra;

/**
 * @author Lorenzo
 * @version 1.0
 * Classe che permette di gestire una palestra
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Iterator;

public class GestionePalestra {
    private HashMap<String,TipoAbbonamento> abbonamentoMembri;
    private ArrayList<ClasseDiAllenamento> listaClassiDiAllenamento;

    /**
     * Metodo che restituisce l'abbonamentoMembri
     * @return abbonamentoMembri
     */

    public HashMap<String, TipoAbbonamento> getAbbonamentoMembri() {
        return abbonamentoMembri;
    }

    /**
     * Metodo che sette l'abbonamento membri
     * @param abbonamentoMembri
     */

    public void setAbbonamentoMembri(HashMap<String, TipoAbbonamento> abbonamentoMembri) {
        this.abbonamentoMembri = abbonamentoMembri;
    }

    /**
     * Metodo che restuisce la listaClassiDiAllenamento
     * @return listaClassiDiAllenamento
     */

    public ArrayList<ClasseDiAllenamento> getListaClassiDiAllenamento() {
        return listaClassiDiAllenamento;
    }

    /**
     * Metodo che setta la listaCLassiDiAllenamento
     * @param listaClassiDiAllenamento
     */

    public void setListaClassiDiAllenamento(ArrayList<ClasseDiAllenamento> listaClassiDiAllenamento) {
        this.listaClassiDiAllenamento = listaClassiDiAllenamento;
    }

    /**
     * Metodo che costruisce una palestra
     * @param abbonamentoMembri
     * @param listaClassiDiAllenamento
     */

    public GestionePalestra(HashMap<String,TipoAbbonamento> abbonamentoMembri, ArrayList<ClasseDiAllenamento> listaClassiDiAllenamento){
        this.abbonamentoMembri = abbonamentoMembri;
        this.listaClassiDiAllenamento = listaClassiDiAllenamento;
    }

    /**
     * Metodo che permette la registrazione di un nuovo membro in palestra
     * @param membro
     */

    public void registrazioneNuoviMembri(Membro membro){
        this.abbonamentoMembri.put(membro.getId(),membro.getTipoAbbonamento());
    }

    /**
     * Metodo che assegna al parametro membro il parametro tipoAbbonamento, aggiornando lo stato di abbonamento
     * @param membro
     * @param tipoAbbonamento
     */

    public void assegnaAbbonamento(Membro membro, TipoAbbonamento tipoAbbonamento){
        membro.setTipoAbbonamento(tipoAbbonamento);
    }

    /**
     * Metodo che aggiunge una nuova classe di allenamento in palestra
     * @param classeDiAllenamento
     */

    public void aggiungiClasseDiAllenamento(ClasseDiAllenamento classeDiAllenamento){
        this.listaClassiDiAllenamento.add(classeDiAllenamento);
    }

    /**
     * Metodo che assegna un istruttore ad una classe
     * @param classeDiAllenamento
     * @param istruttore
     */

    public void assegnaIstruttore(ClasseDiAllenamento classeDiAllenamento, Istruttore istruttore){
        classeDiAllenamento.setIstruttoreAssegnato(istruttore);
    }

    /**
     * Metodo che verifica se l'abbonamento di un membro e' scaduto o meno
     * @param membro
     * @return
     */

    public boolean verificaValiditaAbbonamento(Membro membro){
        return membro.getTipoAbbonamento() != TipoAbbonamento.scaduto;
    }

    /**
     * Metodo che permette ad un membro di iscriversi ad una classe di allenamento. Nel caso in cui l'abbonamento del membro sia scaduto o nel caso in cui il membro risulti gia' iscritto, vengono stampati dei messaggi di errore dalle classi Exception
     * @param classeDiAllenamento
     * @param membro
     * @throws ClassePienaException
     * @throws AbbonamentoNonValidoException
     */

    public void iscrizioneMembriNelleCLassi(ClasseDiAllenamento classeDiAllenamento, Membro membro) throws ClassePienaException, AbbonamentoNonValidoException{
        if (  classeDiAllenamento.getListaMembriIscritti().size()+1 > classeDiAllenamento.getCapacita() )
            throw new ClassePienaException("Capacita' massima raggiunta per la classe "+classeDiAllenamento.getNomeDellaClasse());
        if ( !this.verificaValiditaAbbonamento(membro) )
            throw new AbbonamentoNonValidoException("L'utente "+membro.getNome()+" "+membro.getCognome()+" abbonamento scaduto");
        classeDiAllenamento.aggiungiMembro(membro);
    }

    /**
     * Metodo che restituisce i primi due caratteri del orario di una classe (il formato dell'orario e' GiornoDellaSettimana Orario, quindi sono sufficienti solo due lettere per identificare un giorno: Lunedi' - > Lu, Martedi' -> Ma,...)
     * @param classeDiAllenamento
     * @return
     */

    public String ottieniOrario(ClasseDiAllenamento classeDiAllenamento){
        return classeDiAllenamento.getOrario().substring(0,1);
    }

    /**
     * Stampa l'elenco dei programmi in palestra del giorno passato come paramentro
     * @param giorno
     */

    public void stampaProgrammiPerGiorno(String giorno){
        System.out.println("Stampa il programma di "+giorno+":");
        for ( int i=0; i<this.listaClassiDiAllenamento.size(); i++ ){
            if ( giorno.substring(0,1).equals(ottieniOrario(this.listaClassiDiAllenamento.get(i))) )
                this.listaClassiDiAllenamento.get(i).stampaClasse();
        }
        System.out.println("\n");
    }

    /**
     * Metodo che calcola la media dei voti di una classe e stampa il risultato
     * @param classeDiAllenamento
     * @throws MembroNonIscrittoAllaClasse
     */

    public void mediaClasse(ClasseDiAllenamento classeDiAllenamento) throws MembroNonIscrittoAllaClasse{
        float media = 0;
        Iterator<Entry<String,Float>> new_Iterator = classeDiAllenamento.getListaVoti().entrySet().iterator();

        while ( new_Iterator.hasNext() )
            media = classeDiAllenamento.getListaVoti().get(new_Iterator.next().getKey()) + media;
        media = media/classeDiAllenamento.getListaVoti().size();
        System.out.println("Media recensioni della palestra: "+media+"\n");
    }
}