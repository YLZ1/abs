package Palestra;
/**
 * @author Lorenzo
 * @version 1.0
 * Classe che estende Exceptpion. Stampa un messaggio di errore nel caso in cui una classe abbia raggiunto la sua capacita' massima
 */
public class ClassePienaException extends Exception{
    public ClassePienaException(){}
    public ClassePienaException(String message){
        super(message);
    }
}
