import java.util.Scanner;

public class Vector3D {
    private float v1;
    private float v2;
    private float v3;
    public float getV1(){
        return this.v1;
    }
    public float getV2(){
        return this.v2;
    }
    public float getV3(){
        return this.v3;
    }
    public void setV1(float v1){
        this.v1 = v1;
    }
    public void setV2(float v2){
        this.v2 = v2;
    }
    public void setV3(float v3){
        this.v3 = v3;
    }
    public static Vector3D creaVect(){
        Vector3D v = new Vector3D();
        Scanner in1 = new Scanner(System.in);
        v.setV1(in1.nextFloat());
        Scanner in2 = new Scanner(System.in);
        v.setV2(in2.nextFloat());
        Scanner in3 = new Scanner(System.in);
        v.setV3(in3.nextFloat());
        return v;
    }
    public static void printVect(Vector3D v){
        System.out.println("("+v.getV1()+", "+v.getV2()+", "+v.getV3()+")^T");
    }
    public static Vector3D sum(Vector3D v, Vector3D w){
        Vector3D sum = new Vector3D();
        sum.setV1(v.getV1() + w.getV1());
        sum.setV2(v.getV2() + w.getV2());
        sum.setV3(v.getV3() + w.getV3());
        return sum;
    }
    public static float scalarprod(Vector3D v, Vector3D w){
        return v.getV1() * w.getV1() + v.getV2() * w.getV2() + v.getV3() * w.getV3();
    }
    public static Vector3D vectprod(Vector3D v, Vector3D w){
        Vector3D p = new Vector3D();
        p.setV1( v.getV2()*w.getV3() - w.getV2()*v.getV3() );
        p.setV2( - v.getV1()*w.getV3() + w.getV1()*v.getV3() );
        p.setV3( v.getV1()*w.getV2() - w.getV1()*v.getV2() );
        return p;
    }

    public static void main(String[] args) {
        System.out.println("Scegli il vettore (dim 3 su R) v:");
        Vector3D v = creaVect();
        System.out.println("v=");
        printVect(v);

        System.out.println("Scegli il vettore (dim 3 su R) w:");
        Vector3D w = creaVect();
        System.out.println("w=");
        printVect(w);

        System.out.println("v+w=");
        printVect(sum(v, w));
        System.out.println("<v,w>="+scalarprod(v,w));
        System.out.println("v X w=");
        printVect(vectprod(v,w));
    }
}