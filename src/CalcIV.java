import java.util.Scanner;
import java.util.Arrays;
import java.util.*;

public class CalcIV {
    public static void main(String[]args){
        String expr0 = "Inizio";
        String delims = "( )+";
        int ESC = 0;        //Variabile di uscita dal ciclo
        System.out.println("Avvio della calcolatrice.");
        System.out.println("E' possibile semplificare espressioni tra numeri reali (+,-,*,/).");
        System.out.println("Digitare ESC per chiudere il programma.");
        while(ESC == 0) {
            System.out.println("Scrivere l'espressione: ");        // Comando
            Scanner in = new Scanner(System.in);
            expr0 = in.next();
            int len=0;        //lunghezza array di stringhe dell'espressione
            for (int count=0; count < expr0.length(); count ++)
                if ( isSign(expr0.charAt(count)) == 1  )
                    len++;
            len = 2 * len + 1;
            String[] expr = new String[len];
            //Arrays.fill(expr, null);
            //expr = new String[len];
            //Parses string into array list
            List<String> list = new ArrayList<String>(Arrays.asList(expr0.split(delims)));
            System.out.println("Expr: " + list);
            System.out.println(list.get(0));




            if( expr0.equals("ESC") )
                ESC = 1;



        }

    }
    public static int isSign(char c){           //Controlla se c e' il simbolo di un'operazione binaria
        if ( c == '+' || c == '-' )
            return 1;
        else if ( c == '*' || c == '/' )        //Usata per vedere se un'espressione e' o meno un binomio
            return 2;
        else
            return 0;
    }
}