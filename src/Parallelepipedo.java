public class Parallelepipedo{
        private float altezza;
        private float lunghezza;
        private float profondita;

        public float getAltezza(){
            return this.altezza;
        }
        public float getLunghezza(){
            return this.lunghezza;
        }
        public float getProfondita(){
            return this.profondita;
        }
        public void setAltezza(float altezza){
            this.altezza = altezza;
        }
        public void setLunghezza(float lunghezza){
            this.lunghezza = lunghezza;
        }
        public void setProfondita(float profondita){
            this.profondita = profondita;
        }
        public static float volume(Parallelepipedo p){
            return p.getAltezza() * p.getLunghezza() * p.getProfondita();
        }
    }