package Azienda;

public class Impiegato extends Dipendente{
    private String reparto;

    public String getReparto() {
        return reparto;
    }

    public void setReparto(String reparto) {
        this.reparto = reparto;
    }

    public static void stampaInfo(Impiegato impiegato1){
        System.out.println("Nome impiegato: "+impiegato1.getNome());
        System.out.println("Stipendio impiegato: "+impiegato1.getStipendio());
        System.out.println("Reparto: "+impiegato1.getReparto());
    }

}
