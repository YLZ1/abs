package Azienda;

public class Dipendente {
    public String nome;
    public double stipendio;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getStipendio() {
        return stipendio;
    }

    public void setStipendio(double stipendio) {
        this.stipendio = stipendio;
    }

    public static void stampaInfo(Dipendente dipendente1){
        System.out.println("Nome impiegato: "+dipendente1.getNome());
        System.out.println("Stipendio impiegato: "+dipendente1.getStipendio());
    }
}