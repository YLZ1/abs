package Scacchi;

public class Tile {
    private boolean occupata;

    public boolean isOccupata() {
        return occupata;
    }

    public void setOccupata(boolean occupata) {
        this.occupata = occupata;
    }

    public Tile(boolean occupata) {
        this.occupata = occupata;
    }
}
