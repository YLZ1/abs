package Scacchi;

public class Map {
    private Tile[][] map;

    public Tile[][] getMap() {
        return map;
    }

    public void setMap(Tile[][] map) {
        this.map = map;
    }

    public Map(Tile[][] map) {
        this.map = map;
    }
}