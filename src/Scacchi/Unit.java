package Scacchi;

public abstract class Unit {
    private String nome;
    private int salute;
    private int forza;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getSalute() {
        return salute;
    }

    public void setSalute(int salute) {
        this.salute = salute;
    }

    public int getForza() {
        return forza;
    }

    public void setForza(int forza) {
        this.forza = forza;
    }
    public Unit(String nome, int salute, int forza){
        this.nome = nome;
        this.salute = salute;
        this.forza = forza;
    }
}