package Scacchi;

import java.util.ArrayList;

public class Army {
    private ArrayList<Unit> army;

    public ArrayList<Unit> getArmy() {
        return army;
    }

    public void setArmy(ArrayList<Unit> army) {
        this.army = army;
    }
    public Army(ArrayList<Unit> army){
        this.army = army;
    }
}
