package Universita;

import java.util.ArrayList;

public class Professore extends Persona{
    private String nome;
    private String cognome;
    private int ID;
    private ArrayList<String> listaCorsi;

    public Professore(String nome, String cognome, int ID, ArrayList<String> listaCorsi){
        this.nome = nome;
        this.cognome = cognome;
        this.ID = ID;
        this.listaCorsi = listaCorsi;
    }

    public void aggiungiCorso(Professore professore, String corso){
        professore.getListaCorsi().add(corso);
    }

    public void rimuoviCorso(Professore professore, String corso){
        professore.getListaCorsi().remove(corso);
    }
}