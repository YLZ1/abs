package Universita;

import java.util.ArrayList;

public class Corso {
    private String titolo;
    private int codice;
    private Professore professore;
    private ArrayList<Studente> listaStudenti;
    private int numeroMaxStudenti;

    public String getTitolo() {
        return titolo;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    public int getCodice() {
        return codice;
    }

    public void setCodice(int codice) {
        this.codice = codice;
    }

    public Professore getProfessore() {
        return professore;
    }

    public void setProfessore(Professore professore) {
        this.professore = professore;
    }

    public ArrayList<Studente> getListaStudenti() {
        return listaStudenti;
    }

    public void setListaStudenti(ArrayList<Studente> listaStudenti) {
        this.listaStudenti = listaStudenti;
    }

    public int getNumeroMaxStudenti() {
        return numeroMaxStudenti;
    }

    public void setNumeroMaxStudenti(int numeroMaxStudenti) {
        this.numeroMaxStudenti = numeroMaxStudenti;
    }
    public Corso(String titolo, int codice, Professore professore, ArrayList<Studente> listaStudenti, int numeroMaxStudenti){
        this.titolo = titolo;
        this.codice = codice;
        this.professore = professore;
        this.listaStudenti = listaStudenti;
        this.numeroMaxStudenti = numeroMaxStudenti;
    }

    public void aggiungiStudente(Studente studente){
        this.listaStudenti().add(studente);
    }
    public void rimuoviStudente(Studente studente){
        this.listaStudenti().remove(studente);
    }
    public void assegnaProfessore(Corso corso, Professore professore){
        corso.setProfessore(professore);
    }
}