package Universita;

import java.util.ArrayList;

public class Studente extends Persona{
    private String nome;
    private String cognome;
    private int ID;
    private int annoDiImmatricolazione;
    private ArrayList<String> listaCorsi;

    public int getAnnoDiImmatricolazione() {
        return annoDiImmatricolazione;
    }

    public void setAnnoDiImmatricolazione(int annoDiImmatricolazione) {
        this.annoDiImmatricolazione = annoDiImmatricolazione;
    }

    public Studente(String nome, String cognome, int ID, int annoDiImmatricolazione, ArrayList<String> listaCorsi){
        this.nome = nome;
        this.cognome = cognome;
        this.ID = ID;
        this.annoDiImmatricolazione = annoDiImmatricolazione;
        this.listaCorsi = listaCorsi;
    }

    public void iscrizioneCorso(Studente studente, String corso){
        studente.getListaCorsi().add(corso);
        studente.setListaCorsi(getListaCorsi());
    }

    public void rimozioneCorso(Studente studente, String corso){
        studente.getListaCorsi().remove(corso);
    }
}