package Universita;

import java.util.ArrayList;

public abstract class Persona {
    private String nome;
    private String cognome;
    private int ID;
    private ArrayList<String> listaCorsi;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public ArrayList<String> getListaCorsi() {
        return listaCorsi;
    }

    public void setListaCorsi(ArrayList<String> listaCorsi) {
        this.listaCorsi = listaCorsi;
    }
}
