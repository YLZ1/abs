package Universita;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) throws Exception{
        ArrayList<Studente> listaStudenti = new ArrayList<>();
        ArrayList<Professore> listaProfessori = new ArrayList<>();
        ArrayList<Corso> listaCorsi = new ArrayList<>();
        Universita universita = new Universita(listaStudenti, listaProfessori, listaCorsi);

        ArrayList<String> listaCorsi1 = new ArrayList<>();
        universita.registraStudente("Ryosuke", "Ozeki", 45625, 2024, listaCorsi1);

        ArrayList<String> listaCorsi2 = new ArrayList<>();
        universita.registraStudente("Hyungseop", "Kim", 45626, 2024, listaCorsi2);

        ArrayList<String> listaCorsi3 = new ArrayList<>();
        universita.registraProfessore("Thomas", "Nikolaus", 15, listaCorsi3);

        ArrayList<Studente> listaStudenti4 = new ArrayList<>();
        Corso corso1 = universita.creaCorso("K-Theorie der Adischer Raume", 12, universita.getListaProfessori().get(0), listaStudenti4, 3);

        //universita.aggiungiStudenteAlCorso(universita.getListaStudenti().get(0), corso1);
        //universita.aggiungiStudenteAlCorso(universita.getListaStudenti().get(1), corso1);

        //universita.stampaListaStudenti(universita.getListaCorsi().get(0));
        //universita.setListaProfessori(universita.getListaProfessori());
    }
}