package Universita;

import java.util.ArrayList;

public class Universita {
    private ArrayList<Studente> listaStudenti;
    private ArrayList<Professore> listaProfessori;
    private ArrayList<Corso> listaCorsi;

    public ArrayList<Studente> getListaStudenti() {
        return listaStudenti;
    }

    public void setListaStudenti(ArrayList<Studente> listaStudenti) {
        this.listaStudenti = listaStudenti;
    }

    public ArrayList<Professore> getListaProfessori() {
        return listaProfessori;
    }

    public void setListaProfessori(ArrayList<Professore> listaProfessori) {
        this.listaProfessori = listaProfessori;
    }

    public ArrayList<Corso> getListaCorsi() {
        return listaCorsi;
    }

    public void setListaCorsi(ArrayList<Corso> listaCorsi) {
        this.listaCorsi = listaCorsi;
    }

    public Universita(ArrayList<Studente> listaStudenti, ArrayList<Professore> listaProfessori, ArrayList<Corso> listaCorsi){
        this.listaStudenti = listaStudenti;
        this.listaProfessori = listaProfessori;
        this.listaCorsi = listaCorsi;
    }

    public void registraStudente(String nome, String cognome, int ID, int annoDiImmatricolazione, ArrayList<String> listaCorsi){
        Studente studente = new Studente(nome,cognome,ID,annoDiImmatricolazione,listaCorsi);
        this.getListaStudenti().add(studente);
    }

    public void registraProfessore(String nome, String cognome, int ID, ArrayList<String> listaCorsi){
        Professore professore = new Professore(nome,cognome,ID,listaCorsi);
        this.getListaProfessori().add(professore);
    }

    public Corso creaCorso(String titolo, int codice, Professore professore, ArrayList<Studente> listaStudenti, int numeroMaxStudenti){
        Corso corso = new Corso(titolo, codice, professore, listaStudenti, numeroMaxStudenti);
        return corso;
    }

    public void aggiungiStudenteAlCorso(Studente studente, Corso corso) throws Exception{
        if (corso.getListaStudenti().size()+1>corso.getNumeroMaxStudenti()){
            throw new Exception("Numero massimo di studenti raggiunto per il corso "+corso.getTitolo());
        }
        if ( studente.getListaCorsi() != null && !studente.getListaCorsi().contains(corso.getTitolo()) ){
            throw new Exception("Studente "+studente.getNome()+" "+studente.getCognome()+" gia' iscritto al corso "+corso.getTitolo());
        }
        studente.iscrizioneCorso(studente, corso.getTitolo());
        corso.aggiungiStudente(studente);
    }

    public void assegnaProfessoreAlCorso(Professore professore, Corso corso){
        professore.aggiungiCorso(professore,corso.getTitolo());
        corso.assegnaProfessore(corso,professore);
    }

    public void stampaListaStudenti(Corso corso){
        System.out.println("Elenco degli studenti iscritti al corso "+corso.getTitolo()+":");
        for ( int i=0; i<corso.getListaStudenti().size(); i++ )
            System.out.println(corso.getListaStudenti().get(i).getNome()+" "+corso.getListaStudenti().get(i).getCognome());
    }

    public void stampaElencoCorsiProfessore(Professore professore){
        System.out.println("Elenco dei corsi insegnati dal Prof. "+professore.getNome()+" "+professore.getCognome());
        for ( int i=0; i<professore.getListaCorsi().size(); i++ )
            System.out.println(professore.getListaCorsi().get(i));
    }

}
