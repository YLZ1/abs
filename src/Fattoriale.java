import java.util.Scanner;

public class Fattoriale {
    public static void main(String[] args) {
        System.out.println("Digitare un numero intero: ");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        System.out.println(n+"! = "+fatt(n));
    }
    public static int fatt(int n){
        if ( n == 0 || n == 1 )
            return 1;
        return n * fatt(n-1);
    }
    public static int fattfor(int n){
        if ( n == 0 || n == 1 )
            return 1;
        int k = n;
        for ( int i=k-1; i >= 1; i--){
            n = n * i;
        }
        return n;
    }
}