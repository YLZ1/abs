import java.util.Scanner;
import java.util.Arrays;
import java.lang.*;

public class Calcolatrice{
    public static void main(String[]args){
        String expr = "Inizio";
        int ESC = 0;        //Variabile di uscita dal ciclo
        System.out.println("Avvio della calcolatrice.");
        System.out.println("E' possibile eseguire oprazioni binarie tra numeri reali (+,-,*,/).");
        System.out.println("Digitare ESC per chiudere il programma.");
        while(ESC == 0){

            float i, j; //operandi
            char command;   //comando operazione
            System.out.println("Scrivere l'espressione: ");        // Comando
            Scanner in = new Scanner(System.in);
            expr = in.next();

            if( expr.equals("ESC") )
                ESC = 1;
            else {

                char[] ch = new char[expr.length()];                  //Trasformare l'espressione in un array

                for (int count = 0; count < expr.length(); count++) {
                    ch[count] = expr.charAt(count);
                }

                int lOp1 = 0;                                            //Calcolo la lunghezza dell'array del primo operando
                for (int count = 0; isSign(ch[count]) == 0; count++) {
                    lOp1++;
                }

                char[] Op1 = new char[lOp1];                  //Definisco e inializzo l'array Operando 1

                for (int count = 0; count < lOp1; count++) {
                    Op1[count] = ch[count];
                }

                String Ns1 = new String(Op1);               //Converto Op1 in una stringa, per poi
                i = Float.parseFloat(Ns1);                  //convertirlo in un float

                command = ch[lOp1];                         //Operazione (+, -, *, /)

                int lOp2 = 0;                                            //Lunghezza array Op2
                for (int count = lOp1+1; count < ch.length; count++) {
                    lOp2++;
                }

                char[] Op2 = new char[lOp2];                  //Array Operando 2

                for (int count = 0; count < lOp2; count++) {
                    Op2[count] = ch[count+lOp1+1];
                }

                String Ns2 = new String(Op2);
                j = Float.parseFloat(Ns2);

                System.out.println( BinOp(command, i, j) );                       //Eseguo l'operazione binaria
            }
        }
    }
    public static int isSign(char c){           //Controlla se c e' il simbolo di un'operazione binaria
        if ( c == '+' || c == '-' || c == '*' || c == '/')
            return 1;
        return 0;
    }
    public static float BinOp(char command, float i, float j){               //Esegue l'operazione binaria
        switch (command) {
            case '+':                                                         //Somma
                return i + j;
            case '-':                                                         //Sottrazione
                return i - j;
            case '*':                                                         //Moltiplicazione
                return i * j;
            case '/':                                                         //Divisione
                if (j != 0)
                    return i / j;
                else
                    System.out.println("Operazione non valida in R");         //Il Denominatore deve essere != 0
                return Float.MAX_VALUE;
        }
        return Float.MAX_VALUE;
    }
}