package abs;

public class main {
    public static void main(String[] args) {
        double stipendioBase = 999999999;
        Impiegato impiegato1 = new Impiegato(stipendioBase, "Donald J. Trump", 1234);
        System.out.println("Nome: "+impiegato1.getNome());
        System.out.println("Codice: "+impiegato1.getCodice());
        System.out.println("Stipendio: "+impiegato1.calcolaStipendio());
    }
}
