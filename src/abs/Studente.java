package abs;
import java.util.*;

public class Studente {
    private String nome;
    private int voto;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getVoto() {
        return voto;
    }

    public void setVoto(int voto) {
        this.voto = voto;
    }

    public Studente( String nome, int voto ){
        this.nome = nome;
        this.voto = voto;
    }

    public static void stampa(List<Studente> lista){
        for ( int i=0; i<lista.size(); i++ )
            System.out.println(lista.get(i).getNome());
    }

    public static double mediavoti(List<Studente> lista){
        double media = 0;
        for ( int i=0; i<lista.size(); i++ )
            media += lista.get(i).getVoto();
        return media/lista.size();
    }

    public static void main(String[] args) {
        List<Studente> lista = new ArrayList<>();
        Studente studente1 = new Studente("Donald J. Trump", 30);
        Studente studente2 = new Studente("Joe Biden", 19);
        Studente studente3 = new Studente("Ronald Reagan", 26);
        Studente studente4 = new Studente("Dwight Eisenhower", 23);
        lista.add(studente1);
        lista.add(studente2);
        lista.add(studente3);
        lista.add(studente4);
        System.out.println("Elenco studenti:");
        stampa(lista);
        System.out.println("Media voti: "+mediavoti(lista));
        lista.clear();
    }
}