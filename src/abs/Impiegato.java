package abs;

public class Impiegato extends Dipendente{
    private double stipendioBase;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getCodice() {
        return codice;
    }

    public void setCodice(int codice) {
        this.codice = codice;
    }

    public double getStipendioBase() {
        return stipendioBase;
    }

    public void setStipendioBase(double stipendioBase) {
        this.stipendioBase = stipendioBase;
    }

    public Impiegato(double stipendioBase, String nome, int codice){
        this.stipendioBase = stipendioBase;
        this.nome = nome;
        this.codice = codice;
    }

    @Override
    public double calcolaStipendio() {
        return stipendioBase + (10)*(stipendioBase/100);
    }
}