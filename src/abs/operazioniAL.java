/*
Le operazioni che possono essere eseguite sugli ArrayList in Java includono:

1. **Creazione**: Creare un nuovo ArrayList.
   - `ArrayList<E> list = new ArrayList<>();`

2. **Aggiunta di elementi**:
   - `add(E e)`: Aggiunge l'elemento specificato alla fine della lista.
   - `add(int index, E element)`: Inserisce l'elemento specificato alla posizione indicata.

3. **Rimozione di elementi**:
   - `remove(Object o)`: Rimuove la prima occorrenza dell'elemento specificato dalla lista.
   - `remove(int index)`: Rimuove l'elemento alla posizione specificata.

4. **Modifica di elementi**:
   - `set(int index, E element)`: Sostituisce l'elemento alla posizione specificata con l'elemento specificato.

5. **Ricerca**:
   - `indexOf(Object o)`: Restituisce l'indice della prima occorrenza dell'elemento specificato, o -1 se la lista non contiene l'elemento.
   - `lastIndexOf(Object o)`: Restituisce l'indice dell'ultima occorrenza dell'elemento specificato, o -1 se la lista non contiene l'elemento.

6. **Iterazione**:
   - Tramite Iterator: `Iterator<E> iterator()`, che permette di iterare gli elementi in sequenza.
   - Tramite loop for-each: per iterare senza utilizzare direttamente un iteratore.

7. **Controllo della presenza di elementi**:
   - `contains(Object o)`: Restituisce true se la lista contiene l'elemento specificato.

8. **Dimensione e capacità**:
   - `size()`: Restituisce il numero di elementi presenti nella lista.
   - Non c'è un metodo diretto per verificare la capacità, ma la capacità viene automaticamente aumentata quando necessario.

9. **Pulizia della lista**:
   - `clear()`: Rimuove tutti gli elementi dalla lista.

10. **Conversione in array**:
    - `toArray()`: Converte la lista in un array.

11. **Clonazione**:
    - `clone()`: Restituisce una copia superficiale della lista.

12. **Slicing**:
    - Non esiste un metodo diretto per lo slicing, ma è possibile utilizzare `subList(int fromIndex, int toIndex)` per ottenere una vista della porzione della lista.

13. **Ordinamento**:
    - `Collections.sort(List<T> list)`: Ordina la lista secondo l'ordine naturale degli elementi o tramite un Comparator fornito.

14. **Ricerca binaria**:
    - `Collections.binarySearch(List<? extends Comparable<? super T>> list, T key)`: Permette di effettuare una ricerca binaria nella lista, che deve essere ordinata.

Nota: Gli esempi di codice sopra menzionati presuppongono l'importazione della classe ArrayList e altre classi di utilità quando necessario.


Esempi implementativi per le operazioni sugli ArrayList in Java:

1. Creazione:
   ```java
   ArrayList<String> list = new ArrayList<>();
   ```

2. Aggiunta di elementi:
   ```java
   list.add("Elemento1");
   list.add(0, "Elemento0"); // Aggiunge "Elemento0" all'inizio della lista
   ```

3. Rimozione di elementi:
   ```java
   list.remove("Elemento0"); // Rimuove "Elemento0" dalla lista
   list.remove(0); // Rimuove l'elemento alla posizione 0
   ```

4. Modifica di elementi:
   ```java
   list.set(0, "NuovoElemento"); // Modifica l'elemento in posizione 0
   ```

5. Ricerca:
   ```java
   int index = list.indexOf("Elemento"); // Trova l'indice di "Elemento"
   ```

6. Iterazione: NB FAR VEDERE ANCHE ITERATOR
   ```java
   for(String elemento : list) {
       System.out.println(elemento);
   }
   ```

7. Controllo della presenza di elementi:
   ```java
   boolean contiene = list.contains("Elemento"); // Verifica se "Elemento" è presente
   ```

8. Dimensione e capacità:
   ```java
   int dimensione = list.size(); // Ottiene la dimensione della lista
   ```

9. Pulizia della lista:
   ```java
   list.clear(); // Rimuove tutti gli elementi
   ```

10. Conversione in array:
    ```java
    Object[] array = list.toArray(); // Converte in array
    ```

11. Clonazione:
    ```java
    ArrayList<String> copia = (ArrayList<String>) list.clone(); // Crea una copia della lista
    ```

12. Slicing (tramite subList):
    ```java
    List<String> sottoLista = list.subList(1, 3); // Ottiene una vista della sotto-lista
    ```

13. Ordinamento:
    ```java
    Collections.sort(list); // Ordina la lista
    ```

14. Ricerca binaria:
    ```java
    int posizione = Collections.binarySearch(list, "ElementoCercato"); // Esegue ricerca binaria
    ``
 */