package abs;

public abstract class Dipendente {
    protected String nome;
    protected int codice;

    public abstract double calcolaStipendio();
}
