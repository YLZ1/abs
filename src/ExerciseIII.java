public class ExerciseIII {
    public static void main(String[]args){
        int m = 20;
        double n = -45.55;
        char o = 'A';
        long p = 15000000000L;
        // Operazioni con operatori unari
        int r1 = ++m * 3;
        double r2 = n++ + --m;
        int r3 = o--;
        long r4 = -p;
        // Operazioni miste con cast
        double r5 = (double)(++o * m) / p;
        int r6 = (int)(--n) / m;
        long r7 = p % ++o;
        System.out.println(r1);
        System.out.println(r2);
        System.out.println(r3);
        System.out.println(r4);
        System.out.println(r5);
        System.out.println(r6);
        System.out.println(r7);
    }
}
