package studente;

public class Studente {
    private String nome;
    private String cognome;
    private int matricola;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public int getMatricola() {
        return matricola;
    }

    public void setMatricola(int matricola) {
        this.matricola = matricola;
    }

    public Studente (String nome, String cognome, int matricola){
        this.nome = nome;
        this.cognome = cognome;
        this.matricola = matricola;
    }

    public static void stampaInfo(Studente studente1){
        System.out.println("Nome: "+studente1.getNome());
        System.out.println("Cognome: "+studente1.getCognome());
        System.out.println("Numero di matricola: "+studente1.getMatricola());
    }
}