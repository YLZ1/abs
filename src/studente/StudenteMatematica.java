package studente;

public class StudenteMatematica extends Studente{
    private String corsoDiStudi;
    public StudenteMatematica(String nome, String cognome, int matricola, String corsoDiStudi){
        super(nome,cognome,matricola);
        this.corsoDiStudi = corsoDiStudi;
    }

    public String getCorsoDiStudi() {
        return corsoDiStudi;
    }

    public void setCorsoDiStudi(String corsoDiStudi) {
        this.corsoDiStudi = corsoDiStudi;
    }

    public static void stampaInfo(StudenteMatematica studente1){
        System.out.println("Nome: "+studente1.getNome());
        System.out.println("Cognome: "+studente1.getCognome());
        System.out.println("Numero di matricola: "+studente1.getMatricola());
        System.out.println("Corso di studi: "+studente1.getCorsoDiStudi());
    }
}
