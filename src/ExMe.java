public class ExMe {
    public static boolean charBelongsToString ( char c, String s ){
        for ( int i=0; i<s.length(); i++ ){
            if ( s.charAt(i) == c )
                return true;
        }
        return false;
    }

    public static int countCharInString ( char c, String s ){
        int count = 0;
        for ( int i=0; i<s.length(); i++ ){
            if ( s.charAt(i) == c )
                count++;
        }
        return count;
    }

    public static boolean charOccCompare (char c1, char c2, String s){
        if (countCharInString(c1,s) == countCharInString(c2,s))
            return true;
        return false;
    }

    public static void main(String[] args) {
        String s = "Prova";
        char c = 's';
        System.out.println(charBelongsToString(c,s));
        System.out.println(charBelongsToString('P',s));
        System.out.println(countCharInString('P',s));
        System.out.println(charOccCompare('P','r',s));

        int n = 9; //e’ un intero
        float f = n;	//casting automatico, l’intero n viene convertito in float

        double d = 9.7;	//e’ un double
        int m = (int) d;	//casting manual, m e’ un int ed è inizializzato a 9


    }
}
