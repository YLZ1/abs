import java.util.Scanner;

public class CalcII {
    public static void main(String[]args){
        String command = "Inizio";
        int ESC = 0;
        System.out.println("Avvio della calcolatrice. E' possibile eseguire somme, sottrazioni, moltiplicazioni e divisioni tra numeri reali. Al momento della scelta dell'operazione, digitare ESC per chiudere il programma.");
        while(ESC == 0){

            float i, j; //operandi
            String Ns1, Ns2; //operandi stringhe
            Scanner N1, N2; //scanner degli operandi stringhe

            System.out.println("Scegliere l'operazione (+, -, *, /): ");        // Comando
            Scanner in = new Scanner(System.in);
            command = in.next();

            if( command.equals("ESC") )
                ESC = 1;
            else {
                System.out.println("Selezionare il primo operando: ");        //Scegliere il primo operando;
                N1 = new Scanner(System.in);
                Ns1 = N1.next();
                i = Float.parseFloat(Ns1);

                System.out.println("Selezionare il secondo operando: ");        //Scegliere il secondo operando;
                N2 = new Scanner(System.in);
                Ns2 = N2.next();
                j = Float.parseFloat(Ns2);
                switch (command) {
                    case "+":                                                         //Somma
                        System.out.println(i + j);
                        break;
                    case "-":                                                         //Sottrazione
                        System.out.println(i - j);
                        break;
                    case "*":                                                         //Moltiplicazione
                        System.out.println(i * j);
                        break;
                    case "/":                                                         //Divisione
                        if (j != 0)
                            System.out.println(i / j);
                        else
                            System.out.println("Operazione non valida in R");         //Il Denominatore deve essere != 0
                        break;
                }
            }
        }
    }
}